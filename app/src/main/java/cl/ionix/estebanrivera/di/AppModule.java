package cl.ionix.estebanrivera.di;

import android.content.Context;

import javax.inject.Singleton;

import cl.ionix.estebanrivera.App;
import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {
    private App app;

    public AppModule(App app) {
        this.app = app;
    }

    @Provides
    @Singleton
    public App provideApplication(){
        return  app;
    }

    @Provides
    @Singleton
    public Context provideContext(){
        return app.getApplicationContext();
    }
}
