package cl.ionix.estebanrivera;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import javax.inject.Inject;

import cl.ionix.estebanrivera.customview.DialogCustom;
import cl.ionix.estebanrivera.data.model.User;
import cl.ionix.estebanrivera.databinding.MainBinding;
import cl.ionix.estebanrivera.presenter.main.MainContract;
import cl.ionix.estebanrivera.presenter.main.MainPresenter;
import cl.ionix.estebanrivera.presenter.main.di.MainPresenterModule;
import cl.ionix.estebanrivera.util.Util;

public class MainActivity extends AppCompatActivity implements MainContract.View {

    private MainBinding binding;
    private User user = null;

    @Inject
    MainPresenter presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        injectComponents();
        attachListener();
        presenter.onAttach(this);
    }

    void injectComponents() {
        ((App) getApplication()).getAppComponent().plus(new MainPresenterModule(this)).inject(this);
    }

    public void attachListener() {

        binding.wrapperPagoFacil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogCustom.input_pay(MainActivity.this, "", new DialogCustom.ClickListener() {
                    @Override
                    public void response(String value) {
                        //rut = value;
                        presenter.fechtData(true, Util.encrypt(Util.formatRut(value)));
                    }

                    @Override
                    public void done() {

                    }

                    @Override
                    public void error(String msg) {
                        Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        binding.wrapperEstaciones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (user != null) {
                    presenter.addUser(true, user);
                }
            }
        });
    }

    @Override
    public void setLoading(boolean loading) {
        binding.progress.setVisibility(loading ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showData(User user) {
        if (user != null) {
            this.user = user;
            DialogCustom.check(this, "", user.getDetail().getEmail(), user.getDetail().getPhoneNumber());
        }
    }

    @Override
    public void userAddedSuccessfully(String userId) {
        DialogCustom.check(this,
                getString(R.string.user_register),
                String.format(getString(R.string.user_register_msg), userId),
                "");
    }

    @Override
    public void showLoadingError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }
}