package cl.ionix.estebanrivera.data.di;

import javax.inject.Singleton;

import cl.ionix.estebanrivera.data.remote.ApiPlaceholderService;
import cl.ionix.estebanrivera.data.remote.ApiService;
import cl.ionix.estebanrivera.util.Config;
import cl.ionix.estebanrivera.util.DefaultSchedulerProvider;
import cl.ionix.estebanrivera.util.SchedulerProvider;
import dagger.Module;
import dagger.Provides;

@Module
public class ApiModule {

    @Provides
    @Singleton
    public ApiService provideAPiService(){
        return new ApiService.Factory().create(Config.URL_API);
    }

    @Provides
    @Singleton
    public ApiPlaceholderService provideAPiPlaceholderService(){
        return new ApiPlaceholderService.Factory().create(Config.URL_API_PLACEHOLDER);
    }

    @Provides
    @Singleton
    public SchedulerProvider provideSchedulerProvider(){
        return new DefaultSchedulerProvider();
    }
}
