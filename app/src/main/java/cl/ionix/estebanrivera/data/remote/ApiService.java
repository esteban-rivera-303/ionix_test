package cl.ionix.estebanrivera.data.remote;

import cl.ionix.estebanrivera.data.remote.response.ResponseUserByRut;
import io.reactivex.Observable;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {

    @GET("search")
    Observable<ResponseUserByRut> getByRut(@Query("rut") String query);

    class Factory {
        public ApiService create(String base_url) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(base_url)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            return retrofit.create(ApiService.class);
        }
    }
}
