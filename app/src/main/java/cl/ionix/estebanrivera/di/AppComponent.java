package cl.ionix.estebanrivera.di;

import javax.inject.Singleton;

import cl.ionix.estebanrivera.data.di.ApiModule;
import cl.ionix.estebanrivera.data.di.TestRespositoryModule;
import cl.ionix.estebanrivera.presenter.main.di.MainComponent;
import cl.ionix.estebanrivera.presenter.main.di.MainPresenterModule;
import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, TestRespositoryModule.class, ApiModule.class})
public interface AppComponent {
    MainComponent plus(MainPresenterModule module);
}
