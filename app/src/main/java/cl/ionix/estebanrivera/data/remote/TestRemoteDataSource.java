package cl.ionix.estebanrivera.data.remote;

import com.google.gson.JsonObject;

import javax.inject.Inject;
import javax.inject.Singleton;

import cl.ionix.estebanrivera.data.TestDataSource;
import cl.ionix.estebanrivera.data.model.User;
import cl.ionix.estebanrivera.data.remote.response.ResponseAddUser;
import cl.ionix.estebanrivera.data.remote.response.ResponseUserByRut;
import io.reactivex.Observable;

@Singleton
public class TestRemoteDataSource implements TestDataSource {

    private ApiService apiService;
    private ApiPlaceholderService apiPlaceholderService;

    @Inject
    public TestRemoteDataSource(ApiService apiService, ApiPlaceholderService apiPlaceholderService) {
        this.apiService = apiService;
        this.apiPlaceholderService = apiPlaceholderService;
    }

    @Override
    public Observable<ResponseUserByRut> getByRut(String rut) {
        return apiService.getByRut(rut);
    }

    @Override
    public Observable<ResponseAddUser> addUser(User user) {
        JsonObject body = new JsonObject();
        JsonObject data = new JsonObject();
        data.addProperty("title", user.getName());
        data.addProperty("email", user.getDetail().getEmail());
        data.addProperty("phone_number", user.getDetail().getPhoneNumber());
        body.add("body", data);
        return apiPlaceholderService.addUser(data);
    }
}
