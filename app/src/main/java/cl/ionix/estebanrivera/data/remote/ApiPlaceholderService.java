package cl.ionix.estebanrivera.data.remote;

import com.google.gson.JsonObject;

import cl.ionix.estebanrivera.data.remote.response.ResponseAddUser;
import cl.ionix.estebanrivera.data.remote.response.ResponseUserByRut;
import io.reactivex.Observable;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;

import retrofit2.http.POST;


public interface ApiPlaceholderService {

    @POST("users")
    Observable<ResponseAddUser> addUser(@Body JsonObject jsonObject);

    class Factory {
        public ApiPlaceholderService create(String base_url) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(base_url)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            return retrofit.create(ApiPlaceholderService.class);
        }
    }
}
