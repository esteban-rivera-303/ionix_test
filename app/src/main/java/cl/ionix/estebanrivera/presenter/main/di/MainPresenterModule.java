package cl.ionix.estebanrivera.presenter.main.di;

import cl.ionix.estebanrivera.data.di.ScreenScoped;
import cl.ionix.estebanrivera.presenter.main.MainContract;
import dagger.Module;
import dagger.Provides;

@Module
public class MainPresenterModule {
    private MainContract.View view;

    public MainPresenterModule(MainContract.View view) {
        this.view = view;
    }

    @Provides
    @ScreenScoped
    public MainContract.View provideView() {
        return view;
    }

}
