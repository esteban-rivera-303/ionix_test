package cl.ionix.estebanrivera.presenter.main.di;

import cl.ionix.estebanrivera.MainActivity;
import cl.ionix.estebanrivera.data.di.ScreenScoped;
import dagger.Subcomponent;


@ScreenScoped
@Subcomponent(modules = {MainPresenterModule.class})
public interface MainComponent {
    void inject(MainActivity activity);
}