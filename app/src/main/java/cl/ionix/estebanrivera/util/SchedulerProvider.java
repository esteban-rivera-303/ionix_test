package cl.ionix.estebanrivera.util;

import java.util.concurrent.Executor;

import io.reactivex.Scheduler;

public interface  SchedulerProvider {

    Scheduler io();

    Scheduler mainThread();

    Scheduler computation();

    Scheduler trampoline();

    Scheduler from(Executor executor);
}
