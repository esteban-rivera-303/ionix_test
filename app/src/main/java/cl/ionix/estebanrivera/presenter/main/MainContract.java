package cl.ionix.estebanrivera.presenter.main;

import com.globallogic.estebanrivera.presenter.base.BasePresenter;
import com.globallogic.estebanrivera.presenter.base.BaseView;

import java.util.List;

import cl.ionix.estebanrivera.data.model.User;

public interface MainContract {

    interface Presenter extends BasePresenter<MainContract.View> {

        void fechtData(boolean showLoading, String rut);
        void addUser(boolean showLoading, User user);


    }

    interface View extends BaseView {

        void setLoading(boolean loading);

        void showData(User user);

        void userAddedSuccessfully(String userId);

        void showLoadingError( String error);
    }
}
