package cl.ionix.estebanrivera.util;

public class Config {
    public static final String URL_API = "https://sandbox.ionix.cl/test-tecnico/";
    public static final String URL_API_PLACEHOLDER = "https://jsonplaceholder.typicode.com/";
}
