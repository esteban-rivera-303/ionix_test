package cl.ionix.estebanrivera.data;

import cl.ionix.estebanrivera.data.model.User;
import cl.ionix.estebanrivera.data.remote.response.ResponseAddUser;
import cl.ionix.estebanrivera.data.remote.response.ResponseUserByRut;
import io.reactivex.Observable;

public interface TestDataSource {

    Observable<ResponseUserByRut> getByRut(String rut);
    Observable<ResponseAddUser> addUser(User user);
}
