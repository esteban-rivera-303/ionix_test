package cl.ionix.estebanrivera.presenter.main;

import javax.inject.Inject;

import cl.ionix.estebanrivera.data.TestRespository;
import cl.ionix.estebanrivera.data.model.User;
import cl.ionix.estebanrivera.data.remote.response.ResponseAddUser;
import cl.ionix.estebanrivera.data.remote.response.ResponseUserByRut;
import cl.ionix.estebanrivera.util.SchedulerProvider;
import io.reactivex.Observable;

public class MainPresenter implements MainContract.Presenter {

    private TestRespository respository;
    private SchedulerProvider schedulerProvider;
    private MainContract.View view = null;

    @Inject
    public MainPresenter(TestRespository respository, SchedulerProvider schedulerProvider) {
        this.respository = respository;
        this.schedulerProvider = schedulerProvider;
    }

    @Override
    public void fechtData(boolean showLoading, String rut) {
        view.setLoading(showLoading);
        Observable<ResponseUserByRut> disposable = respository.getByRut(rut);
        disposable.observeOn(schedulerProvider.mainThread())
                .subscribeOn(schedulerProvider.io()).subscribe(
                responseUserByRut -> {
                    view.setLoading(false);
                    if (responseUserByRut.getResult() != null) {
                        if (responseUserByRut.getResult().getItems() != null) {
                            if (!responseUserByRut.getResult().getItems().isEmpty()) {
                                if (responseUserByRut.getResult().getItems().size() > 1) {
                                    view.showData(responseUserByRut.getResult().getItems().get(1));
                                } else {
                                    view.showData(responseUserByRut.getResult().getItems().get(0));
                                }

                            } else {
                                view.showLoadingError("El rut no esta registrado");
                            }
                        } else {
                            view.showLoadingError("error 2");
                        }
                    } else {
                        view.showLoadingError("error 3");
                    }

                },
                throwable -> {
                    view.setLoading(false);
                    view.showLoadingError("error");
                }
        );

    }

    @Override
    public void addUser(boolean showLoading, User user) {
        view.setLoading(showLoading);
        Observable<ResponseAddUser> disposable = respository.addUser(user);
        disposable.observeOn(schedulerProvider.mainThread())
                .subscribeOn(schedulerProvider.io())
                .subscribe(
                        responseAddUser -> {
                            view.setLoading(false);
                            if (responseAddUser.getId() != null) {
                                view.userAddedSuccessfully(String.valueOf(responseAddUser.getId()));
                            }else{
                                view.showLoadingError("Error 23");
                            }
                        },
                        throwable -> {
                            view.setLoading(false);
                            view.showLoadingError("Error");
                        }
                );

    }

    @Override
    public void onAttach(MainContract.View view) {
        this.view = view;
    }

    @Override
    public void onDetach() {

    }
}
