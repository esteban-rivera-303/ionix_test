package com.globallogic.estebanrivera.presenter.base

import androidx.fragment.app.Fragment
import cl.ionix.estebanrivera.R


open class BaseFragment : Fragment() {



    /**
     * Starts a fragment and adds it to back stack.
     *
     * @param fragment - fragment to start
     */


    fun startFragment(fragment: Fragment, anim: Int) {

        when (anim) {

            FADE -> activity!!.supportFragmentManager
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.nothing)
                .replace(R.id.wrapper, fragment, fragment.javaClass.simpleName)
                .addToBackStack(null)
                .commit()
            SIDE_UP -> activity!!.supportFragmentManager
                .beginTransaction()
                .setCustomAnimations(R.anim.move_up_in, R.anim.move_down_out, R.anim.fade_in, R.anim.fade_out)
                .replace(R.id.wrapper, fragment, fragment.javaClass.simpleName)
                .addToBackStack(null)
                .commit()
            SIDE_LEFT -> activity!!.supportFragmentManager
                .beginTransaction()
                .setCustomAnimations(
                    R.anim.move_right_in_activity,
                    R.anim.move_left_out_activity,
                    R.anim.move_left_in_activity,
                    R.anim.move_right_out_activity
                )
                .replace(R.id.wrapper, fragment, fragment.javaClass.simpleName)
                .addToBackStack(null)
                .commit()
            else -> {
            }
        }
    }

    companion object {
        val FADE = 0
        val SIDE_UP = 1
        val SIDE_LEFT = 2
    }
}