package cl.ionix.estebanrivera.data;

import javax.inject.Inject;
import javax.inject.Singleton;

import cl.ionix.estebanrivera.data.di.Remote;
import cl.ionix.estebanrivera.data.model.User;
import cl.ionix.estebanrivera.data.remote.TestRemoteDataSource;
import cl.ionix.estebanrivera.data.remote.response.ResponseAddUser;
import cl.ionix.estebanrivera.data.remote.response.ResponseUserByRut;
import io.reactivex.Observable;

@Singleton
public class TestRespository {
    @Remote
    private TestDataSource remoteDataSource;

    @Inject
    public TestRespository(TestRemoteDataSource remoteDataSource) {
        this.remoteDataSource = remoteDataSource;
    }

    public Observable<ResponseUserByRut> getByRut(String rut) {
        return this.remoteDataSource.getByRut(rut.trim());
    }

    public Observable<ResponseAddUser> addUser(User user) {
        return this.remoteDataSource.addUser(user);
    }
}
