package cl.ionix.estebanrivera.data.di;

import androidx.transition.Transition;

import javax.inject.Singleton;

import cl.ionix.estebanrivera.data.TestDataSource;
import cl.ionix.estebanrivera.data.remote.ApiPlaceholderService;
import cl.ionix.estebanrivera.data.remote.ApiService;
import cl.ionix.estebanrivera.data.remote.TestRemoteDataSource;
import dagger.Module;
import dagger.Provides;

@Module
public class TestRespositoryModule {

    @Provides
    @Singleton
    @Remote
    public TestDataSource provideRemoteDataSource(ApiService apiService, ApiPlaceholderService apiPlaceholderService) {
        return new TestRemoteDataSource(apiService, apiPlaceholderService);
    }
}
