package cl.ionix.estebanrivera.customview;

import android.app.Dialog;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Window;
import android.view.WindowManager;

import androidx.appcompat.widget.AppCompatButton;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.android.material.textview.MaterialTextView;

import cl.ionix.estebanrivera.R;
import cl.ionix.estebanrivera.util.Util;

public class DialogCustom {

    public static void input_pay(final Context context, String title, ClickListener listener) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_input_pay);
        dialog.setCancelable(true);

        final boolean[] is_valid = {false};
        MaterialTextView txt = dialog.findViewById(R.id.title);
        TextInputLayout til = dialog.findViewById(R.id.tilRut);
        TextInputEditText box_rut = dialog.findViewById(R.id.boxRut);

        if (!title.isEmpty()) {
            txt.setText(title);
        }

        box_rut.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (Util.validarRut(s.toString())) {
                    til.setEndIconDrawable(R.drawable.ic_check);
                    is_valid[0] = true;
                } else {
                    til.setEndIconDrawable(R.drawable.ic_error_small);
                    is_valid[0] = false;
                }

            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;


        ((AppCompatButton) dialog.findViewById(R.id.bt_close)).setOnClickListener(v -> {

            if (!box_rut.getText().toString().isEmpty()) {
                if (is_valid[0]) {
                    dialog.dismiss();
                    listener.response(box_rut.getText().toString());
                } else {
                    listener.error(context.getString(R.string.rut_invalid));
                }
            } else {
                dialog.dismiss();
                listener.done();
            }
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    public static void check(final Context context, String title, String email, String number) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_check);
        dialog.setCancelable(true);

        MaterialTextView txt = dialog.findViewById(R.id.title);
        MaterialTextView tv_email = dialog.findViewById(R.id.tvEmail);
        MaterialTextView tv_number = dialog.findViewById(R.id.tvNumber);

        if (!title.isEmpty()) {
            txt.setText(title);
        }

        tv_email.setText(email);

        tv_number.setText(number);


        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;


        ((AppCompatButton) dialog.findViewById(R.id.bt_close)).setOnClickListener(v -> {
            dialog.dismiss();
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }


    public interface BaseClickListener {
        void done();

        void error(String msg);
    }

    public interface ClickListener extends BaseClickListener {
        void response(String rut);
    }

}
