package cl.ionix.estebanrivera;

import android.app.Application;

import cl.ionix.estebanrivera.di.AppComponent;
import cl.ionix.estebanrivera.di.AppModule;
import cl.ionix.estebanrivera.di.DaggerAppComponent;

public class App extends Application {

    public AppComponent getAppComponent() {
        return appComponent;
    }

    private AppComponent appComponent = null;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }
}
